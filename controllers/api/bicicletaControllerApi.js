var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function(req, res)  {
    res.status(200).json({
        bicicletas: Bicicleta.allBicis
    })
}
exports.bicicleta_create = function(req, res)  {
    var bicicleta = new Bicicleta(req.body.id, req.body.color, req.body.modelo, [req.body.lat, req.body.lng]);
    Bicicleta.add(bicicleta);

    res.status(200).json({
        Response: bicicleta
    })
}
exports.bicicleta_delete = function(req, res)  {
    console.log(req.body.id);
    Bicicleta.removeById(req.body.id);

    res.status(200).json({
        Response: "OK"
    });
}