var Bicicleta = function (id, color, modelo, ubicacion){
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = function ()    {
    return "id:" + this.id + "color: " + this.color;
}

Bicicleta.allBicis = [];
Bicicleta.add = function (aBici) {
    Bicicleta.allBicis.push(aBici);
}

Bicicleta.findByid = function(id)  {
    var bici = Bicicleta.allBicis.find(x => x.id == id);
    if (bici)
        return bici;
    
    throw new Error("no existe bici con id "+id);
}

Bicicleta.removeById = function(id)    {
    for (var i = 0;i<Bicicleta.allBicis.length;i++) {
        if (Bicicleta.allBicis[i].id == id) {
            Bicicleta.allBicis.splice(i,1);
            console.log("Removiendo "+i);
        }
    }
    console.log("Removido OK");
}

Bicicleta.printAllBicis = function() {
    for (var i = 0;i<Bicicleta.allBicis.length;i++) {
        console.log(Bicicleta.allBicis[i].id);
    }
}
var a = new Bicicleta(1,"rojo", "tx910", [-34.6012424,-58.3861497]);
var b = new Bicicleta(2,"rojo", "tx910", [-34.11111,-58.11111]);

Bicicleta.add(a);
Bicicleta.add(b);
//Bicicleta.removeById(1);
//Bicicleta.printAllBicis();

module.exports = Bicicleta;